package com.example.userpage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }

    private val email = emailEditText.text.toString()
    private val password = passwordTextView.text.toString()

    private fun init() {
        signInButton.setOnClickListener() {
            nextPage()
        }
    }

    private fun nextPage() {
        if (email.isNotEmpty() && password.isNotEmpty()) {
            val intent = Intent(this, UserPageActivity::class.java)
            startActivity(intent)
        }else {
            Toast.makeText(this, "please fill all containers", Toast.LENGTH_SHORT).show()
        }
    }
}