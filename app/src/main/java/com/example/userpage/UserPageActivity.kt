package com.example.userpage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_user_page.*

class UserPageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_page)
    }
    private fun init() {
        Glide.with(this)
            .load("https://images.unsplash.com/photo-1531804055935-76f44d7c3621?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80")
            .placeholder(R.mipmap.cover_placeholder)
            .error(R.mipmap.cover_placeholder)
            .into(coverImageView)

        Glide.with(this)
            .load("https://www.facebook.com/photo.php?fbid=885424651942579&set=pb.100014251568815.-2207520000..&type=3")
            .placeholder(R.mipmap.profile_picture_placeholder)
            .error(R.mipmap.profile_picture_placeholder)
            .into(profilePictureImageView)
    }
}